
import java.util.Date;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
public class AllergyWristband extends WristBand{
    List<Medication> meds;
    
    AllergyWristband(String name, Doctor doc, Date dob, Barcode code, List<Medication> meds){
       super(name,doc,dob,code);
       this.meds = meds;
    }

    @Override
    public String toString() {
        return super.toString() + "meds=" + meds ;
    }
   
}
