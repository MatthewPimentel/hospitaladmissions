
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
public class Simulation {
    public static void main(String [] args){
        Doctor doc = new Doctor ("Dr. Tom");
        Barcode code = new Barcode("DSAD938DSW2");
        Medication med = new Medication ("Penicilin");
        List<Medication> meds = new ArrayList<Medication>();
        meds.add(med);
        String patientName = "John Doe";
        Date dob = new Date();
        AllergyWristband band = new AllergyWristband(patientName, doc , dob, code, meds);
        
        List<WristBand> bands = new ArrayList<WristBand>();
        bands.add(band);
        
        Patient patient = new Patient(patientName, bands);
        List<Patient> patients = new ArrayList<Patient>();
        patients.add(patient);
        
        ResearchGroup group = new ResearchGroup(patients);
        
        System.out.println(group);
    }
}   
