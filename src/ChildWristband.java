
import java.util.Date;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
public class ChildWristband extends WristBand {
    List<Parent> parents;
    
    ChildWristband(String name, Doctor doc, Date dob, Barcode code, List<Parent> parents){
        super(name, doc, dob, code);
        this.parents = parents;
    }
    
}
