
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
public class Patient {
    String patientName;
    List<WristBand> bands;
    
    Patient(String patientName, List<WristBand> bands){
        this.patientName = patientName;
        this.bands = bands;
    }

    @Override
    public String toString() {
        return "patientName= " + patientName + ", bands=" + bands;
    }
    
    
}
