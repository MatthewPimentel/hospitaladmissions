
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
public class WristBand {
    private String name;
    private Doctor doc;
    private Date dob;
    private Barcode code;
    
    WristBand(String name, Doctor doc, Date dob, Barcode code){
        this.name = name;
        this.doc = doc;
        this.dob = dob;
        this.code = code;
    }

    @Override
    public String toString() {
        return "WristBand" + "name=" + name + ", doc=" + doc + ", dob=" + dob + ", code=" + code;
    }
    
    
}
