
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
public class ResearchGroup {
    List<Patient> patients;
    
    ResearchGroup(List<Patient> patients){
        this.patients = patients;
    }

    @Override
    public String toString() {
        return "ResearchGroup " + "patients=" + patients;
    }
    
    
}
