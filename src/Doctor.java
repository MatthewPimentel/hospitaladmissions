/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
class Doctor extends Person{
    
    Doctor(String name){
        super(name);
    }
    
    public String toString(){
        return "Doctor: " + super.getName();
    }
}
